const math = require('./math'),
    date = require('./date');

module.exports = {math, date};