const max = (object) => {

    let values = null;

    if (Array.isArray(object)) {
        values = object.map(item => parseInt(item));
    } else if (typeof object === 'object' && object !== null) {
        values = Object.values(object).map(item => parseInt(item));
    } else {
        throw new Error('invalid arguments');
    }

    return values.filter((item) => {
        return Number.isInteger(item);
    }).reduce((a, b) => {
        return Math.max(a, b);
    });

};

module.exports = {
    max
};