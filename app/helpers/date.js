const date = new Date(Date.now());

const getNow = () => {
    return date;
};

const getNowStamp = () => {
    return Date.now();
};

const getDay = () => {
    return getNow().getDate();
};

const getMonth = () => {
    return getNow().getMonth() + 1;
};

const getYear = () => {
    return getNow().getFullYear();
};

const getFullDate = () => {
    return `${getYear()}/${getMonth()}/${ getDay()}`;
};


module.exports = {
    getNow,
    getNowStamp,
    getDay,
    getMonth,
    getYear,
    getFullDate
};