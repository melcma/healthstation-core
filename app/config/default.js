const config = {
    websites: [
        {url: 'http://adrianpiwowarczyk.com'},
        {url: 'http://zizzi.co.uk'},
        {url: 'http://nojolondon.co.uk'},
        {url: 'http://grandpigalle.com'},
        {url: 'http://kanada-ya.com'},
        {url: 'http://goodmanrestaurants.com'},
        {url: 'http://yahoo.com'},
        {url: 'http://test.com'}
    ]
};

module.exports = config;