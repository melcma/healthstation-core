const {getFullDate} = require('../../helpers/date');

const now = new Date().toJSON().slice(0, 10).split('-').join('/');

test(`returns today's date`, () => {
    expect(getFullDate()).toBe(now);
});