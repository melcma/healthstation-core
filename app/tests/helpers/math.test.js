const {max} = require('../../helpers/math');

const testingObj = {
    '0': '-1000',
    'foo': 'foo',
    '100': 100,
    '60': 60,
    '-5': -5
};

const testingArr = [
    1, 4, -4, 9, 'g45', 55, [1, 2], 12, 0, 'b'
];

test(`returns maximum value of ${JSON.stringify(testingObj)}, 100`, () => {
    expect(max(testingObj)).toBe(100);
});

test(`returns maximum value of ${JSON.stringify(testingArr)}, 55`, () => {
    expect(max(testingArr)).toBe(55);
});

test('returns an invalid argument error', () => {
    expect(() => {
        max(null)
    }).toThrow();
});