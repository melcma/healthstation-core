const express = require('express'),
    routes = require('./controllers/routes');

const app = express();

app.set('view options', {layout: false});
app.use(express.static(__dirname + '/views'));

routes(app);

app.use((req, res, next) => {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();

});


module.exports = app;