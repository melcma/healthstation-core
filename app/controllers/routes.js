const analyzeUrl = require('./urls/analyzeUrl'),
    getLogLatest = require('./logs/getLogLatest'),
    getLogByDay = require('./logs/getLogByDay');

module.exports = (app) => {
    app.get('/', (req, res) => {
        res.render('index');
    });

    app.get('/analyze/:url', analyzeUrl);

    app.get('/logs/:url', getLogLatest);

    app.get('/logs/:url/:year/:month/:day', getLogByDay);
};