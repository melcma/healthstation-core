const fs = require('fs');

const {max} = require('../helpers/math');

function showLatest(url) {

    let year,
        month,
        day;

    const path = `${__dirname}/../data/logs/${url}`;

    year = max(fs.readdirSync(path));
    month = max(fs.readdirSync(`${path}/${year}`));
    day = max(fs.readdirSync(`${path}/${year}/${month}`));

    return {
        year,
        day,
        month,
    }

}

module.exports = showLatest;