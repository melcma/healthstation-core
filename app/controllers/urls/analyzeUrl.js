const mkdirp = require('mkdirp'),
    axios = require('axios'),
    https = require('https');

const {
    getFullDate,
    getNowStamp
} = require('./../../helpers/date');

const saveLog = require('./../../controllers/saveLog');

module.exports = (req, res) => {

    let data = {};

    const startTracking = Date.now();

    const query = https.request({
        hostname: req.params.url,
        port: 443,
        path: '/',
        method: 'GET'
    }, () => {
        data.https = true;
        testIt('https://' + req.params.url);
    }).on('error', () => {
        data.https = false;
        testIt('http://' + req.params.url);
    });

    query.end();

    function testIt(url) {

        const axiosQuery = axios.create({
            timeout: 3000
        });

        axiosQuery.get(url)
            .then((fetchRes) => {
                getSiteInfo(req.params.url, fetchRes);
            })
            .catch((error) => {
                if (error.response) {
                    getSiteInfo(req.params.url, error.response);
                } else if (error.request) {
                    getSiteInfo(req.params.url, {'status': 400});
                } else {
                    res.sendStatus(400);
                }
            })
    }


    function getSiteInfo(url, result) {
        const endTracking = Date.now();

        data.status = result.status;
        data.time = (endTracking - startTracking) / 1000;

        const path = `${__dirname}/../../data/logs/${url}/${getFullDate()}`;

        mkdirp(path, (err) => {

            if (err) res.sendStatus(400);

            saveLog(path, `${getNowStamp()}.json`, JSON.stringify(data));

            res.redirect(`/logs/${req.params.url}`);

        });

    }

};