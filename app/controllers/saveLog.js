const fs = require('fs');

function saveLog(path, filename, data) {

    fs.writeFile(path + '/' + filename, data, (err) => {
        if (err) throw err;
    });

}

module.exports = saveLog;